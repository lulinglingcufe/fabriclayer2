# Quick Start

The Merkle tree based multi-origin query is realized in **merkletreequery.js**.

We are able to acquire the latency of conditional queries against Fabric Layer 2 with **conditionalquery.js**.

We use the Merkle tree implementation  of  https://github.com/miguelmota/merkletreejs